package dk.rm.integration.bookplan.ris;

import static org.apache.camel.component.hl7.HL7.convertLFToCR;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.hl7.HL7DataFormat;
import org.apache.camel.processor.validation.PredicateValidationException;
import org.apache.camel.spi.DataFormat;

import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.parser.EncodingNotSupportedException;
import ca.uhn.hl7v2.protocol.TransportException;
import ca.uhn.hl7v2.validation.ValidationContext;
import ca.uhn.hl7v2.validation.impl.DefaultValidation;

/**
 * Created by akoufoudakis on 26/10/16.
 */
public class BookplanRisRoute extends RouteBuilder {

    public void configure() {

        getContext().setTracing(true);

        DataFormat hl7 = new HL7DataFormat();

        ValidationContext defaultContext = new DefaultValidation();

        onException(PredicateValidationException.class, DataTypeException.class, EncodingNotSupportedException.class)
                .handled(true)
                .log("${exception}")
                .to("amq:queue:{{queuename.dlq}}");

        onException(TransportException.class, Exception.class)
                .maximumRedeliveries("{{max.redeliveries}}")
                .redeliveryDelay("{{redelivery.delay}}")
                .log("${exception}");

        /**
         * Dequeue the message, unmarshal it to the HL7 format, forward to RIS, using HAPI.
         */
        from("amq:queue:{{queuename}}")
                .routeId("siu-queue-mllp")
                .unmarshal(hl7)
                .setBody(convertLFToCR())
                .processRef("forwardProcessor");

    }

}
