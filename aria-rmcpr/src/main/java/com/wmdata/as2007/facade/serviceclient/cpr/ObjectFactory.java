
package com.wmdata.as2007.facade.serviceclient.cpr;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.wmdata.as2007.facade.serviceclient.cpr package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetYdelsesModtager_QNAME = new QName("http://cpr.serviceclient.facade.as2007.wmdata.com/", "getYdelsesModtager");
    private final static QName _GetYdelsesModtagerResponse_QNAME = new QName("http://cpr.serviceclient.facade.as2007.wmdata.com/", "getYdelsesModtagerResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.wmdata.as2007.facade.serviceclient.cpr
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetYdelsesModtager }
     * 
     */
    public GetYdelsesModtager createGetYdelsesModtager() {
        return new GetYdelsesModtager();
    }

    /**
     * Create an instance of {@link GetYdelsesModtagerResponse }
     * 
     */
    public GetYdelsesModtagerResponse createGetYdelsesModtagerResponse() {
        return new GetYdelsesModtagerResponse();
    }

    /**
     * Create an instance of {@link CprPerson }
     * 
     */
    public CprPerson createCprPerson() {
        return new CprPerson();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetYdelsesModtager }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cpr.serviceclient.facade.as2007.wmdata.com/", name = "getYdelsesModtager")
    public JAXBElement<GetYdelsesModtager> createGetYdelsesModtager(GetYdelsesModtager value) {
        return new JAXBElement<GetYdelsesModtager>(_GetYdelsesModtager_QNAME, GetYdelsesModtager.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetYdelsesModtagerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cpr.serviceclient.facade.as2007.wmdata.com/", name = "getYdelsesModtagerResponse")
    public JAXBElement<GetYdelsesModtagerResponse> createGetYdelsesModtagerResponse(GetYdelsesModtagerResponse value) {
        return new JAXBElement<GetYdelsesModtagerResponse>(_GetYdelsesModtagerResponse_QNAME, GetYdelsesModtagerResponse.class, null, value);
    }

}
