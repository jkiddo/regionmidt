
package com.wmdata.as2007.facade.serviceclient.cpr;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b14002
 * Generated source version: 2.2
 * 
 */
@WebService(name = "CPRServiceBean", targetNamespace = "http://cpr.serviceclient.facade.as2007.wmdata.com/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface CPRServiceBean {


    /**
     * 
     * @param arg0
     * @return
     *     returns com.wmdata.as2007.facade.serviceclient.cpr.CprPerson
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getYdelsesModtager", targetNamespace = "http://cpr.serviceclient.facade.as2007.wmdata.com/", className = "com.wmdata.as2007.facade.serviceclient.cpr.GetYdelsesModtager")
    @ResponseWrapper(localName = "getYdelsesModtagerResponse", targetNamespace = "http://cpr.serviceclient.facade.as2007.wmdata.com/", className = "com.wmdata.as2007.facade.serviceclient.cpr.GetYdelsesModtagerResponse")
    public CprPerson getYdelsesModtager(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

}
