
package com.wmdata.as2007.facade.serviceclient.cpr.midt;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.wmdata.as2007.facade.serviceclient.cpr.midt package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _HentAlleOplysningerForListeResponse_QNAME = new QName("http://midt.cpr.serviceclient.facade.as2007.wmdata.com/", "hentAlleOplysningerForListeResponse");
    private final static QName _HentAlleOplysninger_QNAME = new QName("http://midt.cpr.serviceclient.facade.as2007.wmdata.com/", "hentAlleOplysninger");
    private final static QName _HentAlleOplysningerResponse_QNAME = new QName("http://midt.cpr.serviceclient.facade.as2007.wmdata.com/", "hentAlleOplysningerResponse");
    private final static QName _HentAlleOplysningerForListe_QNAME = new QName("http://midt.cpr.serviceclient.facade.as2007.wmdata.com/", "hentAlleOplysningerForListe");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.wmdata.as2007.facade.serviceclient.cpr.midt
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link HentAlleOplysninger }
     * 
     */
    public HentAlleOplysninger createHentAlleOplysninger() {
        return new HentAlleOplysninger();
    }

    /**
     * Create an instance of {@link HentAlleOplysningerResponse }
     * 
     */
    public HentAlleOplysningerResponse createHentAlleOplysningerResponse() {
        return new HentAlleOplysningerResponse();
    }

    /**
     * Create an instance of {@link HentAlleOplysningerForListe }
     * 
     */
    public HentAlleOplysningerForListe createHentAlleOplysningerForListe() {
        return new HentAlleOplysningerForListe();
    }

    /**
     * Create an instance of {@link HentAlleOplysningerForListeResponse }
     * 
     */
    public HentAlleOplysningerForListeResponse createHentAlleOplysningerForListeResponse() {
        return new HentAlleOplysningerForListeResponse();
    }

    /**
     * Create an instance of {@link Field }
     * 
     */
    public Field createField() {
        return new Field();
    }

    /**
     * Create an instance of {@link FieldContainer }
     * 
     */
    public FieldContainer createFieldContainer() {
        return new FieldContainer();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HentAlleOplysningerForListeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://midt.cpr.serviceclient.facade.as2007.wmdata.com/", name = "hentAlleOplysningerForListeResponse")
    public JAXBElement<HentAlleOplysningerForListeResponse> createHentAlleOplysningerForListeResponse(HentAlleOplysningerForListeResponse value) {
        return new JAXBElement<HentAlleOplysningerForListeResponse>(_HentAlleOplysningerForListeResponse_QNAME, HentAlleOplysningerForListeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HentAlleOplysninger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://midt.cpr.serviceclient.facade.as2007.wmdata.com/", name = "hentAlleOplysninger")
    public JAXBElement<HentAlleOplysninger> createHentAlleOplysninger(HentAlleOplysninger value) {
        return new JAXBElement<HentAlleOplysninger>(_HentAlleOplysninger_QNAME, HentAlleOplysninger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HentAlleOplysningerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://midt.cpr.serviceclient.facade.as2007.wmdata.com/", name = "hentAlleOplysningerResponse")
    public JAXBElement<HentAlleOplysningerResponse> createHentAlleOplysningerResponse(HentAlleOplysningerResponse value) {
        return new JAXBElement<HentAlleOplysningerResponse>(_HentAlleOplysningerResponse_QNAME, HentAlleOplysningerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HentAlleOplysningerForListe }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://midt.cpr.serviceclient.facade.as2007.wmdata.com/", name = "hentAlleOplysningerForListe")
    public JAXBElement<HentAlleOplysningerForListe> createHentAlleOplysningerForListe(HentAlleOplysningerForListe value) {
        return new JAXBElement<HentAlleOplysningerForListe>(_HentAlleOplysningerForListe_QNAME, HentAlleOplysningerForListe.class, null, value);
    }

}
