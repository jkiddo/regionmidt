
package com.wmdata.as2007.facade.serviceclient.cpr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cprPerson complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cprPerson">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="adresseBeskyttet" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="bynavn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coNavn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cprNr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gade" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="koen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="komNr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lokalitet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="navn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="postNr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sikringsgruppe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cprPerson", propOrder = {
    "adresseBeskyttet",
    "bynavn",
    "coNavn",
    "cprNr",
    "gade",
    "koen",
    "komNr",
    "lokalitet",
    "navn",
    "postNr",
    "sikringsgruppe"
})
public class CprPerson {

    protected boolean adresseBeskyttet;
    protected String bynavn;
    protected String coNavn;
    protected String cprNr;
    protected String gade;
    protected String koen;
    protected String komNr;
    protected String lokalitet;
    protected String navn;
    protected String postNr;
    protected String sikringsgruppe;

    /**
     * Gets the value of the adresseBeskyttet property.
     * 
     */
    public boolean isAdresseBeskyttet() {
        return adresseBeskyttet;
    }

    /**
     * Sets the value of the adresseBeskyttet property.
     * 
     */
    public void setAdresseBeskyttet(boolean value) {
        this.adresseBeskyttet = value;
    }

    /**
     * Gets the value of the bynavn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBynavn() {
        return bynavn;
    }

    /**
     * Sets the value of the bynavn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBynavn(String value) {
        this.bynavn = value;
    }

    /**
     * Gets the value of the coNavn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoNavn() {
        return coNavn;
    }

    /**
     * Sets the value of the coNavn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoNavn(String value) {
        this.coNavn = value;
    }

    /**
     * Gets the value of the cprNr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCprNr() {
        return cprNr;
    }

    /**
     * Sets the value of the cprNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCprNr(String value) {
        this.cprNr = value;
    }

    /**
     * Gets the value of the gade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGade() {
        return gade;
    }

    /**
     * Sets the value of the gade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGade(String value) {
        this.gade = value;
    }

    /**
     * Gets the value of the koen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKoen() {
        return koen;
    }

    /**
     * Sets the value of the koen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKoen(String value) {
        this.koen = value;
    }

    /**
     * Gets the value of the komNr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKomNr() {
        return komNr;
    }

    /**
     * Sets the value of the komNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKomNr(String value) {
        this.komNr = value;
    }

    /**
     * Gets the value of the lokalitet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLokalitet() {
        return lokalitet;
    }

    /**
     * Sets the value of the lokalitet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLokalitet(String value) {
        this.lokalitet = value;
    }

    /**
     * Gets the value of the navn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNavn() {
        return navn;
    }

    /**
     * Sets the value of the navn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNavn(String value) {
        this.navn = value;
    }

    /**
     * Gets the value of the postNr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostNr() {
        return postNr;
    }

    /**
     * Sets the value of the postNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostNr(String value) {
        this.postNr = value;
    }

    /**
     * Gets the value of the sikringsgruppe property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSikringsgruppe() {
        return sikringsgruppe;
    }

    /**
     * Sets the value of the sikringsgruppe property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSikringsgruppe(String value) {
        this.sikringsgruppe = value;
    }

}
