
package com.wmdata.as2007.facade.serviceclient.cpr.midt;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for hentAlleOplysningerForListeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="hentAlleOplysningerForListeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cpr-result" type="{http://midt.cpr.serviceclient.facade.as2007.wmdata.com/}fieldContainer" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hentAlleOplysningerForListeResponse", propOrder = {
    "cprResult"
})
public class HentAlleOplysningerForListeResponse {

    @XmlElement(name = "cpr-result")
    protected List<FieldContainer> cprResult;

    /**
     * Gets the value of the cprResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cprResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCprResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FieldContainer }
     * 
     * 
     */
    public List<FieldContainer> getCprResult() {
        if (cprResult == null) {
            cprResult = new ArrayList<FieldContainer>();
        }
        return this.cprResult;
    }

}
