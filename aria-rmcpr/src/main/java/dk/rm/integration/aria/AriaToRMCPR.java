package dk.rm.integration.aria;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.HL7DataFormat;

import com.wmdata.as2007.facade.serviceclient.cpr.CPRServiceBeanService;
import com.wmdata.as2007.facade.serviceclient.cpr.CprPerson;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.v251.datatype.CX;
import ca.uhn.hl7v2.model.v251.message.ADR_A19;
import ca.uhn.hl7v2.model.v251.message.QRY_Q01;
import ca.uhn.hl7v2.model.v251.segment.PID;

public class AriaToRMCPR extends RouteBuilder {
	
//	Example query message
//	MSH|^~\&|ADT|MGH|ADT|MGH|201501271532||QRY^Q01|000341235|P|2.2|||AL|NE|||
//	QRD|201501271532|R|I|0000000IUD|||50^RD|0671630|DEM^X|||T
	
//	Example response message
//	MSH|^~\&|ADT|MGH|ADT|MGH|201501271532||ADR^A19|000341235|P|2.2
//	MSA|AA|000341235|Patient found
//	QRD|201501271532|R|I|0000000IUD|||50^RD|0671630|DEM^X|||T
//	PID|||0671630|WILW320311182016033|WILSON^WILLIAM||19320311|M|||192 HAMPSHIRE ROAD^^^PQ^H9W3N7^^^^66107|66107|514-695-2528|(514)-|E|3|CAT||WILW32031118 16/03|||||||CANA
//	PV1|1|O
//	AL1|1|MA|NKA^NoKnown Allergy (Suspected, ,Rx System)^EXTERNAL||Undetermined Re
//	IN1|1|WILW32031118|MEDI||^^^PQ||||||||20160331

	@Override
	public void configure() throws Exception {
		from("netty4:tcp://{{mllp.host}}:{{mllp.port}}?sync=true&encoder=#hl7encoder&decoder=#hl7decoder").unmarshal(new HL7DataFormat())
				.process(new Processor() {

					@Override
					public void process(Exchange exchange) throws Exception {

						QRY_Q01 hl7Input = exchange.getIn().getBody(QRY_Q01.class);

						ADR_A19 responseMessage = new ADR_A19();
						hl7Input.fillResponseHeader(responseMessage, AcknowledgmentCode.AA);

						responseMessage.getMSH().getSendingApplication().getNamespaceID().setValue("Region Midt ESP");
						responseMessage.getMSH().getVersionID().getVersionID().setValue("2.5.1");
						responseMessage.getMSH().getMessageType().getMessageCode().setValue("ADR");
						responseMessage.getMSH().getMessageType().getTriggerEvent().setValue("A19");
						responseMessage.getMSH().getMessageType().getMessageStructure().setValue("ADR_A19");
						responseMessage.getMSH().getCharacterSet(0).setValue("UNICODE UTF-8");
						responseMessage.getMSH().getMessageProfileIdentifier(0).getEntityIdentifier().setValue("ARIA-PDQ");
						
						responseMessage.getQRD().getQueryDateTime().getTime().setValue(hl7Input.getQRD().getQueryDateTime().getTime().getValueAsCalendar());
						responseMessage.getQRD().getQueryFormatCode().setValue(hl7Input.getQRD().getQueryFormatCode().getValue());
						responseMessage.getQRD().getQueryPriority().setValue(hl7Input.getQRD().getQueryPriority().getValue());
						responseMessage.getQRD().getQueryID().setValue(hl7Input.getQRD().getQueryID().getValue());
						responseMessage.getQRD().getQuantityLimitedRequest().getQuantity().setValue(hl7Input.getQRD().getQuantityLimitedRequest().getQuantity().getValue());
						responseMessage.getQRD().getQuantityLimitedRequest().getUnits().getIdentifier().setValue(hl7Input.getQRD().getQuantityLimitedRequest().getUnits().getIdentifier().getValue());
						responseMessage.getQRD().getWhoSubjectFilter(0).getIDNumber().setValue(hl7Input.getQRD().getWhoSubjectFilter(0).getIDNumber().getValue());
						responseMessage.getQRD().getWhatSubjectFilter(0).getIdentifier().setValue(hl7Input.getQRD().getWhatSubjectFilter(0).getIdentifier().getValue());
						responseMessage.getQRD().getWhatSubjectFilter(0).getText().setValue(hl7Input.getQRD().getWhatSubjectFilter(0).getText().getValue());
						responseMessage.getQRD().getQueryResultsLevel().setValue(hl7Input.getQRD().getQueryResultsLevel().getValue());
						
						responseMessage.getMSA().getAcknowledgmentCode().setValue(AcknowledgmentCode.AA.name());
						responseMessage.getMSA().getMessageControlID().setValue(hl7Input.getMSH().getMessageControlID().getValue());
						responseMessage.getMSA().getTextMessage().setValue("Patient found");
						
						populatePIDSegment(responseMessage.getQUERY_RESPONSE().getPID(), hl7Input.getQRD().getWhoSubjectFilter(0).getIDNumber().getValue());
						exchange.getOut().setBody(responseMessage);
						
					}

					private void populatePIDSegment(PID pid, String ssn) throws DataTypeException {

						CprPerson personInformation = new CPRServiceBeanService().getCPRServiceBeanPort().getYdelsesModtager(ssn);
						
						CX cx = pid.getPatientIdentifierList(0);
						cx.getIDNumber().setValue(personInformation.getCprNr());
						cx.getAssigningAuthority().getUniversalID().setValue("1.2.208.176.1.2");
						cx.getAssigningAuthority().getUniversalIDType().setValue("ISO");
						cx.getIdentifierTypeCode().setValue("NPI");
						
						// Consider splitting names
						pid.getPatientName(0).getGivenName().setValue(personInformation.getNavn());
						
						// And so on ... add address and so fourth
						
					}
				});
	}

}
