package test;

import org.apache.camel.example.guice.CamelGuiceApplicationModule;
import org.apache.camel.guice.ext.RegistryModule;

import com.google.inject.Guice;

public class NotATestHarness {

	public static void main(String[] args) throws Exception {

		System.setProperty("mllp.host", "0.0.0.0");
		System.setProperty("mllp.port", "2575");
		Guice.createInjector(new RegistryModule(), new CamelGuiceApplicationModule());
	}
}
