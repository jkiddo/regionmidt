package dk.rm.integration.ris;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;

/**
 * Created by akoufoudakis on 23/09/16.
 */
public class RouteTest extends CamelTestSupport {

    @Test
    public void testMessage() throws Exception {

        HapiContext context = new DefaultHapiContext();
        String msgString = "MSH|^~\\&|GPMS|CTX||MED2000|200803060953||SIU^S14|20080306953450|P|2.3||||||||\n" +
                "SCH|00331839401|||||58||HLCK^HEALTHCHECK ANY AGE|20|MIN|^^^200803061000 |||||JOHN||||VALERIE|||||ARRIVED|\n" +
                "PID|1||489671|0|SMITH^MICHAEL^||20080205|F|||176215TH STREET^HOUSTON^TX^77306||(832)795-8259|||S|||999999999||||||||||||\n" +
                "PV1|1|O|||||HHR^NGUYENSUSAN MD|^||||||||||||||||||||||||||||||||||| ||||||||||\n" +
                "RGS|1|||\n" +
                "AIL|1||HHR^FPCS NGUYEN, MD||||||||||\n" +
                "NTE|1||1MONTH HLCK^^|\n" +
                "AIP|1||PBN^LISAPORTER|50|||||||||";

        PipeParser parser = context.getPipeParser();
        Message msg = parser.parse(msgString);
        System.out.println("MESSAGE IN STRING FORMAT: " + msg.toString());

        assertTrue(msg.getNames().length > 0);

    }

    @Test
    public void testBodyTransformationBean() throws Exception {
        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("direct:endpoint")
                        .to("mock:endpoint");
            }
        });

        String hl7Msg = "MSH|^~\\&|GPMS|CTX||MED2000|200803060953||SIU^S14|20080306953450|P|2.3||||||||\n" +
                "SCH|00331839401|||||58||HLCK^HEALTHCHECK ANY AGE|20|MIN|^^^200803061000 |||||JOHN||||VALERIE|||||ARRIVED|\n" +
                "PID|1||489671|0|SMITH^MICHAEL^||20080205|F|||176215TH STREET^HOUSTON^TX^77306||(832)795-8259|||S|||999999999||||||||||||\n" +
                "PV1|1|O|||||HHR^NGUYENSUSAN MD|^||||||||||||||||||||||||||||||||||| ||||||||||\n" +
                "RGS|1|||\n" +
                "AIL|1||HHR^FPCS NGUYEN, MD||||||||||\n" +
                "NTE|1||1MONTH HLCK^^|\n" +
                "AIP|1||PBN^LISAPORTER|50|||||||||";

        HapiContext context = new DefaultHapiContext();
        PipeParser parser = context.getPipeParser();
        Message msg = parser.parse(hl7Msg);

        template.sendBody("direct:endpoint", msg);

        MockEndpoint mockEndpoint = getMockEndpoint("mock:endpoint");
        mockEndpoint.expectedMessageCount(1);
        assertMockEndpointsSatisfied();
        Object body = mockEndpoint.getExchanges().get(0).getIn().getBody();
        System.out.println("Canonical name: " + body.getClass().getCanonicalName());

    }


}
