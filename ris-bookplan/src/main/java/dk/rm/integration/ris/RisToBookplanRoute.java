package dk.rm.integration.ris;

import ca.uhn.hl7v2.HL7Exception;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;

/**
 * Created by akoufoudakis on 20/09/16.
 * The route, which receives a message via the tcp call and forwards it to the bookplan via HAPI.
 */
public class RisToBookplanRoute extends RouteBuilder {

    public void configure() {

        /**
         * Log the exception. Do not retry.
         */
        onException(HL7Exception.class)
                .handled(true)
                .log("${body}");

        /**
         * Synchronous mode. Expose the netty4 endpoint and get the message.
         * Send it to the bookplan system from inside the HL7TransformationBean.
         * The beanRef is configured in the blueprint.xml
         */
        from("netty4:tcp://{{mllp.host}}:{{mllp.port}}?sync=true&encoder=#hl7encoder&decoder=#hl7decoder")
                .beanRef("transformationBean", "transformBody")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/hl7-v2; charset=utf-8"));

        /*
        //Asycnhrounous.
        from("netty4:tcp://{{mllp.host}}:{{mllp.port}}?sync=true&encoder=#hl7encoder&decoder=#hl7decoder")
                .to("amq:queue://hl7.over.http.queue");
        //TODO add reply to sender whether message failed or processed successfully.

        from("amq:queue://hl7.over.http.queue")
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                .beanRef("transformationBean", "transformBody");
                //TODO put error negative acknowledgment into a DLQ.
                */

//        from("jetty://http:{{bookplan.host}}:{bookplan.port}}")
//                .setBody(constant("RECEIVED"));
    }

}
