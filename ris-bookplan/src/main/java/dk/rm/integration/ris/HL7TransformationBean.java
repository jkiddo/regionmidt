package dk.rm.integration.ris;

import org.apache.camel.Body;
import org.apache.camel.PropertyInject;
import org.apache.log4j.Logger;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.hoh.api.IReceivable;
import ca.uhn.hl7v2.hoh.api.MessageMetadataKeys;
import ca.uhn.hl7v2.hoh.hapi.client.HohClientSimple;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.parser.PipeParser;

/**
 * Created by akoufoudakis on 23/09/16.
 * Transformation bean, which sends the HL 7 message to the bookplan system.
 * It is declared as a bean in the blueprint.xml.
 */
public class HL7TransformationBean {

    private static Logger logger = Logger.getLogger(HL7TransformationBean.class);

    //the properties below are configured in the blueprint file and can be overriden in the profile properties.
    @PropertyInject("bookplan.host")
    private String host;

    @PropertyInject("bookplan.port")
    private int port;

    @PropertyInject("bookplan.uri")
    private String uri;

    /**
     *
     * @param body
     * @return
     * @throws Exception
     *
     * The method uses, so called, camel parameter binding. It means that the body is "automatically" taken from
     * the camel exchange and cast to an HL7 message. Then, the message is sent to bookplan, using HAPI.
     */
    public Message transformBody(@Body Message body) throws Exception {
        Message message = null;
        try {
            Parser parser = PipeParser.getInstanceWithNoValidation();
            HohClientSimple client = new HohClientSimple(host, port, uri, parser);

            IReceivable<Message> receivable = client.sendAndReceiveMessage(body);

            // receivavle.getRawMessage() provides the response
            message = receivable.getMessage();
            logger.info("Response was:\n" + message.encode());

            // IReceivable also stores metadata about the message
            String remoteHostIp = (String) receivable.getMetadata().get(MessageMetadataKeys.REMOTE_HOST_ADDRESS);
            logger.info("From:\n" + remoteHostIp);


        } catch(Exception e) {
            //throw the new exception, which will be handled by the onException clause of the camel route.
            throw new HL7Exception(e.getMessage());
        }
        return message;
    }

}
