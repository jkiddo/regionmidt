package dk.rm.integration.framework.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.util.Terser;

/**
 * Created by akoufoudakis on 24/10/16.
 */
public class ForwardToRisProcessor implements Processor {

    private static Logger logger = Logger.getLogger(ForwardToRisProcessor.class);

    public void process(Exchange exchange) throws Exception {

        try {

            String location = "";
            Terser terser = new Terser(exchange.getIn().getBody(Message.class));
            String msh5 = terser.get("/.MSH-5");
            String msh6 = terser.get("/.MSH-6");
            if ((msh5 != null) && !msh5.isEmpty())
                location = location.concat(msh5);
            if ((msh6 != null) && !msh6.isEmpty())
                location = location.concat(msh6);
            location = location.replace(" ", "");
            location = location.toLowerCase();
            String risHost = exchange.getContext().resolvePropertyPlaceholders("{{" + location + ".host}}");
            String risPort = exchange.getContext().resolvePropertyPlaceholders("{{" + location + ".port}}");
            logger.info("RIS HOST: " + risHost);
            logger.info("RIS PORT: " + risPort);


            Message hl7Message = exchange.getIn().getBody(Message.class);
            HapiContext context = new DefaultHapiContext();
            Connection connection = context.newClient(risHost, Integer.parseInt(risPort), false);
            logger.info("Sending a message");
            Initiator initiator = connection.getInitiator();
            Message acknowledgement = initiator.sendAndReceive(hl7Message);
            connection.close();
            logger.info("Message sent");
            Terser ackTerser = new Terser(acknowledgement);
            String acknowldgementConfirmation = ackTerser.get("MSA-1");
            if(!acknowldgementConfirmation.contains("AA")) {
                throw new DataTypeException("Application not confirmed");
            }

        } catch(Exception e) {
            throw e;
        }

    }

}
