package dk.rm.integration.bookplan.http;

import static org.apache.camel.component.hl7.HL7.messageConformsTo;
import static org.apache.camel.component.hl7.HL7.terser;

import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.hl7.HL7DataFormat;
import org.apache.camel.processor.validation.PredicateValidationException;
import org.apache.camel.spi.DataFormat;
import org.apache.log4j.Logger;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.validation.ValidationContext;
import ca.uhn.hl7v2.validation.impl.DefaultValidation;

/**
 * Created by akoufoudakis on 06/10/16.
 * The route, which receives a message via HTTP and forwards it to the RIS system.
 */
public class HttpBookplanRoute extends RouteBuilder {

    private static Logger logger = Logger.getLogger(HttpBookplanRoute.class);

    public void configure() {

        getContext().setTracing(true);

        /**
         * Data format, which transforms an input stream (body received through the HTTP)
         * and transforms (unmarshals) it to an HL7 message.
         */
        DataFormat hl7 = new HL7DataFormat();

        /**
         *  Default validation of an HL7 message
         */
        ValidationContext defaultContext = new DefaultValidation();

        /**
         * Exception handling.
         * Do not retry (set handled).
         * In case the exception occurred before sending the message to the queue return the response with 200
         * and add the message to the DLQ queue.
         * In case the exception occurred after putting message to the queue and after, just put it to the DLQ.
         */
        onException(PredicateValidationException.class, HL7Exception.class)
                .handled(true)
                .log("Exception: ${exception}")
                .log("Adding message to the DLQ: ${body}")
                .process(new Processor() {
                    public void process(Exchange exchange) {
                        exchange.getOut().setBody("Failed to process the HL7 message");
                    }
                })
                .to("amq:queue:{{queuename.dlq}}");


        /**
         * Expose the HTTP endpoint and expect the HL7 message.
         * Get the message unmarshal it (from input stream to an HL7 message),
         * validate, marshal back and send to the queue.
         */
        from("jetty:http://{{bookplan.host}}:{{bookplan.port}}/{{bookplan.uri}}")
                .routeId("bookplan-queue-ris")
                .setExchangePattern(ExchangePattern.InOnly)
                .unmarshal(hl7)
                .log("Received message from HTTP: ${body}")
                .validate(messageConformsTo(defaultContext))
                .marshal(hl7)
                .removeHeaders("CamelHttp*")
                .choice()
                    .when(terser("MSH-9").isEqualTo("ADT")).to("amq:queue:{{queuename.adt}}")
                    .when(terser("MSH-9").isEqualTo("SIU")).to("amq:queue:{{queuename.siu}}")
                    .otherwise().to("amq:queue:hl7.queue.dlq")
                .end()
                .unmarshal(hl7)
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        Message hl7Input = exchange.getIn().getBody(Message.class);
                        Message hl7Output = hl7Input.generateACK();
                        //Content type, expected by a HoH client. Probable bug in specs or in HAPI implementation
                        exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/hl7-v2; charset=utf-8");
                        exchange.getIn().setBody(hl7Output);
                }
            }).log("BODY AT THE END: ${body}");

    }

}
