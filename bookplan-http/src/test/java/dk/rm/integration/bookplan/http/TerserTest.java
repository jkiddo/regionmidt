package dk.rm.integration.bookplan.http;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

/**
 * Created by akoufoudakis on 14/10/16.
 */
public class TerserTest {

    @Test
    public void testTerserLocation() throws Exception {

        String hl7String = "MSH|^~\\&|BOOKPLAN||Siemens SynGo||20151211092015+0130||ADT^A10^ADT_A09|709-65387-8588-594|P|2.5||||||UNICODE UTF-8\n" +
                "EVN||20151211092015+0445||||20151211092015+0700\n" +
                "PID|||070683ABXD^^^CPR^PI^&1.2.208.176.1.2&ISO||Doe^Tammy^^^^^L|||F\n" +
                "PV1|1|U|||||||||||||||||appointmentId00081^^^BOOKPLAN";


        HapiContext hapiContext = new DefaultHapiContext();
        PipeParser parser = hapiContext.getPipeParser();
        Message hl7Message = parser.parse(hl7String);
        String message = hl7Message.encode();
        message = message.replace("\n", "\r");
        hl7Message = parser.parse(message);
        Terser terser = new Terser(hl7Message);

        String msh5 = terser.get("PID-3-1");
        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%MSH5: " + msh5);
        assertTrue(msh5 != null);

    }


}
