package dk.rm.integration.bookplan.http;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;
import ca.uhn.hl7v2.validation.ValidationContext;
import ca.uhn.hl7v2.validation.impl.DefaultValidation;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.hl7.HL7;
import org.apache.camel.component.hl7.HL7DataFormat;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.processor.validation.PredicateValidationException;
import org.apache.camel.spi.DataFormat;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;

import java.util.ArrayList;

import static org.apache.camel.component.hl7.HL7.convertLFToCR;
import static org.apache.camel.component.hl7.HL7.messageConformsTo;
import static org.apache.camel.component.hl7.HL7.terser;

/**
 * Created by akoufoudakis on 13/10/16.
 */
public class BookplanRisTest extends CamelTestSupport {

    @Test
    public void testUnmarshal() throws Exception {
        String hl7String = "MSH|^~\\&|||^SIEMENS_VIBORG_RIS|SIEMENS|20161025135317.458+0200||ADT^A01^ADT_A01|54401|4f77f2fe-d972-4ec7-8ad6-63c83450b5bc|2.5.1\r" +
                "PID|||SomeSSN";

        HapiContext hapiContext = new DefaultHapiContext();
        PipeParser parser = hapiContext.getPipeParser();
        Message hl7Message = parser.parse(hl7String);

        DataFormat hl7 = new HL7DataFormat();

        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("direct:input")
                        .process(new Processor() {
                            public void process(Exchange exchange) {
                                System.out.println("$$$$$$$$$BODY: " + exchange.getIn().getBody().getClass().getCanonicalName());
                            }
                        })
                        .to("mock:hl7endpoint");
            }
        });

        template.sendBody("direct:input", hl7Message);
        MockEndpoint mockHl7 = getMockEndpoint("mock:hl7endpoint");
        mockHl7.expectedMessageCount(1);
        mockHl7.message(0).body().isInstanceOf(Message.class);
        assertMockEndpointsSatisfied();

    }

    @Test
    public void testContentBasedHl7Route() throws Exception {

        String adtString = "MSH|^~\\&|BOOKPLAN||Siemens SynGo||20151211092015+0130||ADT^A10^ADT_A09|709-65387-8588-594|P|2.5||||||UNICODE UTF-8\r" +
                "EVN||20151211092015+0445||||20151211092015+0700\r" +
                "PID|||070683ABXD^^^CPR^PI^&1.2.208.176.1.2&ISO||Doe^Tammy^^^^^L|||F\r" +
                "PV1|1|U|||||||||||||||||appointmentId00081^^^BOOKPLAN";

        String siuString = "MSH|^~\\&|^SIEMENS_VIBORG_RIS|SIEMENS|BOOKPLAN||201607051129||SIU^S12^SIU_S12|IWM20160705113214027|P|2.5.1|||||DNK|UNICODE UTF-8\r" +
                "SCH|1|3019799_011\r" +
                "TQ1||||||30^Minutes|20160805113200+0100\r" +
                "PID|||1010100TE3^^^&CPR||Berggren^Nancy To\r" +
                "RGS|1\r" +
                "AIS|1||663002B-CT^CT scanning|||||||Afventes||663002A^Billeddiagnostisk Afdeling Kylle - HEM\r" +
                "NTE|0||CT-skanning af din hals\r" +
                "NTE|1||Med. Sengeafsnit 1^6630301MEDSENG1\r" +
                "AIG|||^SCT";


        HapiContext hapiContext = new DefaultHapiContext();
        PipeParser parser = hapiContext.getPipeParser();

        Message adtMessage = parser.parse(adtString);
        Message siuMessage = parser.parse(siuString);

        context.addRoutes(new RouteBuilder() {
           public void configure() {
               from("direct:input")
                       .choice()
                            .when(terser("MSH-9").isEqualTo("ADT")).to("mock:adt")
                            .when(terser("MSH-9").isEqualTo("SIU")).to("mock:siu")
                            .otherwise().to("mock:dlq");

           }
        });

        template.sendBody("direct:input", adtMessage);
        template.sendBody("direct:input", siuMessage);

        Thread.sleep(500);

        MockEndpoint adt = getMockEndpoint("mock:adt");
        MockEndpoint siu = getMockEndpoint("mock:siu");
        MockEndpoint dlq = getMockEndpoint("mock:dlq");

        adt.expectedMessageCount(1);
        siu.expectedMessageCount(1);
        dlq.expectedMessageCount(0);

        assertMockEndpointsSatisfied();

    }

    @Test
    public void testCamelTester() throws Exception {
        String adtString = "MSH|^~\\&|BOOKPLAN||Siemens SynGo||20151211092015+0130||ADT^A10^ADT_A09|709-65387-8588-594|P|2.5||||||UNICODE UTF-8\n" +
                "EVN||20151211092015+0445||||20151211092015+0700\n" +
                "PID|||070683ABXD^^^CPR^PI^&1.2.208.176.1.2&ISO||Doe^Tammy^^^^^L|||F\n" +
                "PV1|1|U|||||||||||||||||appointmentId00081^^^BOOKPLAN";

        context.addRoutes(new RouteBuilder() {

            DataFormat hl7 = new HL7DataFormat();

            public void configure() {
                from("direct:hl7")
                        .unmarshal(hl7)
                        .setBody(convertLFToCR())
                        .setHeader("PID-3-1", terser("PID-3-1"))
                        .to("mock:output");
            }
        });

        template.sendBody("direct:hl7", adtString);
        Thread.sleep(200);
        MockEndpoint endpoint = getMockEndpoint("mock:output");
        endpoint.message(0).header("PID-3-1").isNotNull();
        assertMockEndpointsSatisfied();

    }

}
