package dk.rm.integration.stub.pas;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;

/**
 * Created by akoufoudakis on 25/10/16.
 */
public class PasStubRoute extends RouteBuilder {

    public void configure() {
        from("jetty:http://0.0.0.0:9191/passtub")
                .log("${body}")
                .setBody(constant("Success"))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));
    }

}
