{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "ssn": {
      "type": "string"
    },
    "time": {
      "type": "string"
    },
    "location": {
      "type": "string"
    },
    "task": {
      "type": "string"
    },
    "status": {
      "enum": [
        "Cancelled",
        "Absent",
        "Awaited",
        "In progress",
        "Finished"
      ]
    },
    "notes": {
      "type": "array",
      "minItems": 0,
      "note": {
        "type": "string"
      }
    }
  },
  "required": [
    "ssn",
    "time",
    "location",
    "task",
    "status"
  ]
}