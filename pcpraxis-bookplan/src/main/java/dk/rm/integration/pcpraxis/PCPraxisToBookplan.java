package dk.rm.integration.pcpraxis;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;

public class PCPraxisToBookplan extends RouteBuilder {

	// http://blog.christianposta.com/camel/easy-rest-endpoints-with-apache-camel-2-14/

	@Override
	public void configure() throws Exception {

		restConfiguration().component("jetty").bindingMode(RestBindingMode.json)
				.dataFormatProperty("prettyPrint", "true").port("{{port}}");

		rest("/Appointment").description("Appointment rest service").consumes("application/json")
				.produces("application/json")

				.post().description("Create an appointment").type(Appointment.class)
				.to("bean:appointmentService?method=createAppointment")

				.put().description("Update an appointment").type(Appointment.class)
				.to("bean:appointmentService?method=updateAppointment")

				.delete().description("Deletes an appointment").type(Appointment.class)
				.to("bean:appointmentService?method=deleteAppointment");
	}

}
