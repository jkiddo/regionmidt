package dk.rm.integration.bookplan.rispas;

import static org.apache.camel.component.hl7.HL7.convertLFToCR;
import static org.apache.camel.component.hl7.HL7.messageConformsTo;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.hl7.HL7DataFormat;
import org.apache.camel.processor.validation.PredicateValidationException;
import org.apache.camel.spi.DataFormat;
import org.apache.log4j.Logger;

import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.parser.EncodingNotSupportedException;
import ca.uhn.hl7v2.protocol.TransportException;
import ca.uhn.hl7v2.validation.ValidationContext;
import ca.uhn.hl7v2.validation.impl.DefaultValidation;

/**
 * Created by akoufoudakis on 10/10/16.
 */
public class BookplanRisPasRoute extends RouteBuilder {

    private static Logger logger = Logger.getLogger(BookplanRisPasRoute.class);

    public void configure() {

        getContext().setTracing(true);

        DataFormat hl7 = new HL7DataFormat();

        ValidationContext defaultContext = new DefaultValidation();

        onException(PredicateValidationException.class, DataTypeException.class, EncodingNotSupportedException.class)
                .handled(true)
                .log("${exception}")
                .to("amq:queue:{{queuename.dlq}}");

        onException(TransportException.class, Exception.class)
                .maximumRedeliveries("{{max.redeliveries}}")
                .redeliveryDelay("{{redelivery.delay}}")
                .log("${exception}");

        from("amq:queue:{{queuename}}")
                .unmarshal(hl7)
                .setBody(convertLFToCR())
                .validate(messageConformsTo(defaultContext))
                .multicast()
                    .pipeline()
                        .processRef("pasProcessor")
                        .log("BODY: ${body}")
                        .to("http://{{pas.host}}:{{pas.port}}/{{pas.path}}").end()
                    .pipeline()
                        .processRef("forwardProcessor")
                        .end();
    }

}
