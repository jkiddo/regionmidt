package dk.rm.integration.bookplan.rispas;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.util.Terser;

/**
 * Created by akoufoudakis on 21/10/16.
 */
public class PasProcessor implements Processor {

    private static Logger logger = Logger.getLogger(PasProcessor.class);

    public void process(Exchange exchange) throws Exception {
        Message hl7Message = exchange.getIn().getBody(Message.class);
        logger.info("BODY: " + hl7Message);
        Terser terser = new Terser(hl7Message);
        String pid31 = terser.get("PID-3-1");

        logger.info("PID-3-1: " + pid31);

        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, String> jsonMap = new HashMap<String, String>();
        jsonMap.put("ssn", pid31);

        exchange.getIn().setBody(objectMapper.writeValueAsString(jsonMap));

    }

}
