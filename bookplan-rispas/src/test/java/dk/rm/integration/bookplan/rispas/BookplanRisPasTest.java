package dk.rm.integration.bookplan.rispas;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;

/**
 * Created by akoufoudakis on 21/10/16.
 */
public class BookplanRisPasTest extends CamelTestSupport {

    @Test
    public void testMulticast() throws Exception {
        context.addRoutes(new RouteBuilder() {
           public void configure() {
               from("direct:input")
                       .multicast()
                        .pipeline().process(new Processor() {
                                public void process(Exchange exchange) {
                                    exchange.getIn().setBody("RIS");
                                }
                            }).to("mock:ris").end()
                        .pipeline().process(new Processor() {
                                public void process(Exchange exchange) {
                                    System.out.println("%%%%%%%%%%%%%%%%%%%%BODY BEFORE PAS: " + exchange.getIn().getBody());
                                    exchange.getIn().setBody("PAS");
                                }
                            }).to("mock:pas").end()
                       .to("mock:default");
           }
        });

        String initial = "INITIAL";

        template.sendBody("direct:input", initial);

        Thread.sleep(500);

        MockEndpoint ris = getMockEndpoint("mock:ris");
        MockEndpoint pas = getMockEndpoint("mock:pas");
        MockEndpoint defaultMock = getMockEndpoint("mock:default");

        ris.expectedMessageCount(1);
        pas.expectedMessageCount(1);
        defaultMock.expectedMessageCount(1);

        assertMockEndpointsSatisfied();

        String risContent = ris.getExchanges().get(0).getIn().getBody(String.class);
        String pasContent = pas.getExchanges().get(0).getIn().getBody(String.class);
        String defaultContent = defaultMock.getExchanges().get(0).getIn().getBody(String.class);

        assertTrue(risContent.contains("RIS"));
        assertTrue(pasContent.contains("PAS"));
        assertTrue(defaultContent.contains(initial));


    }


    @Test
    public void testPasProcessor() throws Exception {

        String hl7String = "MSH|^~\\&|BOOKPLAN||Siemens SynGo||20151211092015+0130||ADT^A10^ADT_A09|709-65387-8588-594|P|2.5||||||UNICODE UTF-8\r" +
                "EVN||20151211092015+0445||||20151211092015+0700\r" +
                "PID|||070683ABXD^^^CPR^PI^&1.2.208.176.1.2&ISO||Doe^Tammy^^^^^L|||F\r" +
                "PV1|1|U|||||||||||||||||appointmentId00081^^^BOOKPLAN";

        HapiContext hapiContext = new DefaultHapiContext();
        PipeParser parser = hapiContext.getPipeParser();
        Message hl7Message = parser.parse(hl7String);

        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("direct:input")
                        .process(new PasProcessor())
                        .to("mock:endpoint");
            }
        });

        template.sendBody("direct:input", hl7Message);
        Thread.sleep(200);
        MockEndpoint mock = getMockEndpoint("mock:endpoint");
        mock.message(0).body().convertToString().contains("070683ABXD");
        assertMockEndpointsSatisfied();

    }


}
